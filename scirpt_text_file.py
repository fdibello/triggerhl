import argparse
import os
import ROOT
import trigger
import numpy as np

f = ROOT.TFile.Open("out.root")
tree = f.Get('tree')



trig_pass_201  = ROOT.TH2F("trig_pass_201","trig_pass_201",  50,-1.4,1.4,50,-3.14,3.14)
trig_total_201 = ROOT.TH2F("trig_total_201","trig_total_201",50,-1.4,1.4,50,-3.14,3.14)

trig_pass_201_pt   = ROOT.TH2F("trig_pass_201_pt","trig_pass_201_pt",20,0,80)
trig_total_201_pt  = ROOT.TH2F("trig_total_201_pt","trig_total_201_pt",20,0,80)


trig_pass_221  = ROOT.TH2F("trig_pass_221","trig_pass_221",  50,-1.4,1.4,50,-3.14,3.14)
trig_total_221 = ROOT.TH2F("trig_total_221","trig_total_221",50,-1.4,1.4,50,-3.14,3.14)

trig_pass_221_pt   = ROOT.TH2F("trig_pass_221_pt","trig_pass_221_pt",20,0,80)
trig_total_221_pt  = ROOT.TH2F("trig_total_221_pt","trig_total_221_pt",20,0,80)


trig_pass_bm  = ROOT.TH2F("trig_pass_bm","trig_pass_bm",  100,-1.4,1.4,100,-3.14,3.14)
trig_total_bm = ROOT.TH2F("trig_total_bm","trig_total_bm",100,-1.4,1.4,100,-3.14,3.14)


trig_pass_bo  = ROOT.TH2F("trig_pass_bo","trig_pass_bo",  100,-1.4,1.4,100,-3.14,3.14)
trig_total_bo = ROOT.TH2F("trig_total_bo","trig_total_bo",100,-1.4,1.4,100,-3.14,3.14)


test_file = open("testfile.txt","w")

i = -1
evn = 0
sel_evn = 0
number_of_tracks = 0
for event in f.tree:
        ncl = len(event.x)
        x = event.x 
        mPhi = event.mPhi 
        time = event.time
        layer = event.layer 
        g_match = event.g_match
        station_eta = event.station_eta
        station_phi = event.station_phi
        station_name = event.station_name
        trk_pt = event.trk_pt
        trk_eta = event.trk_eta
        trk_phi = event.trk_phi
        g_eta = event.g_eta
        g_phi = event.g_phi
        if len(trk_pt) ==0: 
         number_of_tracks = number_of_tracks + 1
         continue
        

        ind = np.asarray(mPhi) > 0
        xt = np.asarray(x)
        lay = np.asarray(layer)

        x4Trig = xt[ind]
        ly4Trig = lay[ind]

        isTrig_2_0_1,isTrig_2_2_1 = trigger.PivotTrigger(x4Trig ,ly4Trig)
                
        if isTrig_2_0_1: 
           trig_pass_201.Fill(trk_eta[0],trk_phi[0])
           trig_pass_201_pt.Fill(trk_pt[0])
        trig_total_201.Fill(trk_eta[0],trk_phi[0])
        trig_total_201_pt.Fill(trk_pt[0])

        if isTrig_2_2_1: 
           trig_pass_221.Fill(trk_eta[0],trk_phi[0])
           trig_pass_221_pt.Fill(trk_pt[0])
        trig_total_221.Fill(trk_eta[0],trk_phi[0])
        trig_total_221_pt.Fill(trk_pt[0])


        evn = evn + 1
        is4Save = 0
        for clu in range(ncl):

         

         if mPhi[clu] == 0:
          #if station_name[clu] == "L":
          #if x[clu] >0 and station_phi[clu] == 3 and station_name[clu] == "L":
          if x[clu] >0  and station_name[clu] == "L":
          #if station_eta[clu] == 3:
#           x_min = 0.385
#           x_max = 1.1
           x_min = 0
           x_max = 1.4
           #bin_x = (x_max-x_min) / 1013.
           bin_x = (x_max-x_min) / (512.)
           x_int = int( (x[clu]-x_min) / bin_x)
           #if( g_match[clu] > 0  ):
           #if( g_match[clu] > 0 and station_name[clu] == "L" ):
           print(x_int,x[clu],station_name[clu],mPhi[clu],time[clu],layer[clu],g_match[clu],int(time[clu]/25.))
           test_file.write("\n"+str(x_int)+" "+str(mPhi[clu])+" "+str(x[clu])+" "+str(layer[clu])+" "+str(g_match[clu])+" "+str( 0 ) ) 
           is4Save = 1
         #test_file.write("\n This is our new text file") 
         #test_file.write("\n and this is another line") 
         #test_file.write("\n Why? Because we can")
        for pt in trk_pt:
         if is4Save == 1: print(" ------ ", pt) 
        if is4Save == 1: test_file.write("\n"+str(x_int)+" "+str(mPhi[clu])+" "+str(time[clu])+" "+str(layer[clu])+" "+str(g_match[clu])+" "+str( trk_pt[0] ) ) 
        if is4Save == 1 : sel_evn = sel_evn + 1
        #if is4Save == 1 and trk_pt[0] != -1: sel_evn = sel_evn + 1
test_file.close()
print("Selcted event = ",sel_evn, float(sel_evn)/float(evn)) 

File_trig = ROOT.TFile("Trig_results.root","recreate")
File_trig.cd()
trig_pass_221.Divide(trig_total_221)
trig_pass_221.Write()
trig_total_221.Write()

trig_pass_201.Divide(trig_total_201)
trig_pass_201.Write()


File_trig.Close()
