import os
import ROOT
import math
import numpy as np


#define time edges
tbmin = [-87.5,-62.5,-37.5,12.5,37.5,62.5]  
tbmax = [-62.5,-37.5,-12.5,37.5,62.5,87.5]

f = ROOT.TFile.Open("/eos/user/m/mcorradi/phase2/DCT/realdata/test_data18_00350121_flatNtuple.root")
outTree = f.Get('outTree')

#Define output file
outputfile = ROOT.TFile('hacked_file.root','recreate')
outputtree = ROOT.TTree('tree','tree')
#Define which outputbranches do we want?


#number of bunch crossing used for the merging
nsum=6
itmax0=1;
itmax1=nsum;

nstrips_bi = 48

for event in f.outTree:
   #loop over the bunch crossings
   simtime = 0.
   counter = 0
   for  it0 in range(itmax0):
    #Ask massimo if this is actually needed
    #simtime +=25.*32
    counter+=1
   
    Hit_nHits = event.Hit_nHits
    old_ly_bi=0
    old_strip_bi=0
    old_eta_bi=0
    old_iphi_bi=0
    old_ly_bm=0
    old_strip_bm=0
    old_eta_bm=0
    old_iphi_bm=0
    for it1 in range(itmax1):
     for i in range(Hit_nHits):
          
           #This trick only works for BM and BO but BI not present in data18
           string = event.Hit_StationName[i]
           isBM = False
           isBO = False
           if  "BM" in string:
            isBM = True
           if  "BO" in string:
            isBO = True
           if isBM == False and isBO == False:
            continue
           phi = event.Hit_StationPhi[i]+0.5*(event.Hit_doubletPhi[i]-1)-0.5
           iphi = (event.Hit_StationPhi[i]-1)*2+(event.Hit_doubletPhi[i]-1);
           iside=0 
           #Ask  Massimo why is this and why 16 
           if (event.Hit_StationEta[i]>0): iside=1
           sidephi = iphi+16*iside
           sidephi_bi = iphi
           tmin=tbmin[it0+it1]
           tmax=tbmax[it0+it1]
           #note the absolute value is defined 
           if (math.fabs(event.Hit_StationEta[i])==3 and event.Hit_Time[i] >= tmin and event.Hit_Time[i] < tmax):
             #hack to emulate BM
             ly_bm=0
             if (event.Hit_gasGap[i]==2): ly_bm+=2;
             if (event.Hit_MeasuresPhi[i]==1): ly_bm+=1;
             if (ly_bm != old_ly_bm or event.Hit_Strip[i] != old_strip_bm or event.Hit_MeasuresPhi[i] != 1 or event.Hit_StationEta[i] != old_eta_bm or iphi==old_iphi_bm):
             #ask Massimo about this transform
               tt_bm = simtime+float(event.Hit_Time[i]-(tmin+12.5)+25*sidephi)
               strip_bm = event.Hit_Strip[i] 
               if (event.Hit_MeasuresPhi[i]==0):
                 strip_bm+=32*(event.Hit_doubletZ[i]-1)
               #Fill BM part  ------------------
               print("BMO ----- ",tt_bm,ly_bm)
               #Ask massimo need to convert from strip number to global coodinates - is it worth?                


             #if for the bm part 
             old_ly_bm=ly_bm
             old_strip_bm=event.Hit_Strip[i]
             old_eta_bm=event.Hit_StationEta[i]
             old_iphi_bm=iphi 
             #######################################

             #now study BI hack
  
             ####################################### 
  
             gg_bm=0
             if (event.Hit_doubletR[i]==2 and event.Hit_gasGap[i]==1): gg_bm=1
             if (event.Hit_doubletR[i]==2 and event.Hit_gasGap[i]==2): gg_bm=2
             ly_bi=gg_bm+3*event.Hit_MeasuresPhi[i]
             if (ly_bi != old_ly_bi or event.Hit_Strip[i] != old_strip_bm or event.Hit_MeasuresPhi[i] != 1 or event.Hit_StationEta[i] != old_eta_bi or iphi==old_iphi_bi):
                 strip_bi = event.Hit_Strip[i]
                 if (event.Hit_MeasuresPhi[i]==0):
                   strip_bi+=32*(event.Hit_doubletZ[i]-1)
                 if (strip_bi<nstrips_bi):
                   tt_bi = simtime+float(event.Hit_Time[i]-(tmin+12.5)+25*sidephi_bi)
                   print("BI ----- ",tt_bi,ly_bi,event.Hit_Time[i])
                 #Fill here info for BI 

             old_ly_bi=ly_bi
             old_strip_bi=event.Hit_Strip[i]
             old_eta_bi=event.Hit_StationEta[i]
             old_iphi_bi=iphi 
            
 
