import ROOT
import numpy as np
import math

#Settori Large, aggiungere repo
#0,   1    2   3   4    5    6
#IM1s,IM2s,IOs,MMs,M10s,M20s, ggBI
#TODO aggiungere finestre tra gasgap
Windows = [
[0.014,0.0165,0.036,0.0085,0.026,0.0205        ,0.00425],
[0.011,0.015,0.033,0.0075,0.024,0.019          ,0.0038],
[0.0105,0.014,0.0305,0.0075,0.0225,0.018       ,0.0036],
[0.0095,0.013,0.027,0.007,0.0195,0.0155        ,0.0041],
[0.009,0.012,0.0255,0.0065,0.0185,0.0155       ,0.0037],
[0.0085,0.0115,0.0235,0.0065,0.017,0.0135      ,0.00345],
[0.007,0.0105,0.0215,0.006,0.0155,0.012        ,0.00305],
[0.007,0.01,0.0205,0.0055,0.0145,0.012         ,0.00325],
[0.0065, 0.0095, 0.019, 0.005, 0.0135, 0.011   ,0.00375],
[0.0075, 0.0095, 0.019, 0.0055, 0.0135, 0.011  ,0.0042],
[0.0075, 0.0095, 0.019, 0.0055, 0.0135, 0.011  ,0.0042],
[0.0075, 0.0095, 0.019, 0.005, 0.013, 0.011    ,0.0042],
[0.007, 0.0095, 0.019, 0.005, 0.014, 0.011     ,0.00375],
[0.007, 0.0105, 0.021, 0.0055, 0.0145, 0.012   ,0.00325],
[0.007, 0.0105, 0.021, 0.006, 0.0155, 0.012    ,0.003],
[0.0085, 0.0115, 0.024, 0.0065, 0.0175, 0.0135 ,0.00345],
[0.009, 0.012, 0.0255, 0.0065, 0.019, 0.0155   ,0.0037],
[0.0095, 0.013, 0.027, 0.007, 0.019, 0.0155    ,0.00405],
[0.0105, 0.014, 0.03, 0.0075, 0.022, 0.0175    ,0.0035],
[0.011,0.0155, 0.033, 0.008, 0.024, 0.0185     ,0.0038],
[0.014, 0.0165, 0.036, 0.0085, 0.026, 0.021    ,0.00425]
]

Windows_small = [
[0.019,0.092,0.045,0.0485,0.029,0.0205        ,0.00425],
[0.0185,0.0225,0.041,0.008,0.025,0.019          ,0.0038],
[0.0175,0.021,0.0365,0.0075,0.0215,0.018       ,0.0036],
[0.017,0.0205,0.034,0.0075,0.0195,0.0155        ,0.0041],
[0.0155,0.019,0.0315,0.0065,0.0185,0.0155       ,0.0037],
[0.015,0.018,0.029,0.006,0.016,0.0135      ,0.00345],
[0.014,0.017,0.0275,0.0055,0.0145,0.012        ,0.00305],
[0.0135,0.0165,0.0265,0.0055,0.0145,0.012         ,0.00325],
[0.013,0.016 , 0.019, 0.0255,0.0055, 0.014   ,0.00375],
[0.013, 0.0155, 0.025, 0.0055, 0.0145, 0.0135, 0.011  ,0.0042],
[0.013, 0.0155, 0.025, 0.0055, 0.0145, 0.011  ,0.0042],
[0.013, 0.0155, 0.0245, 0.0055, 0.0145, 0.011    ,0.0042],
[0.013, 0.0155, 0.025, 0.0055, 0.0145, 0.011     ,0.00375],
[0.0135, 0.015, 0.027, 0.0055, 0.0145, 0.012   ,0.00325],
[0.0135, 0.0155,0.027, 0.006, 0.0155, 0.012    ,0.003],
[0.015, 0.0165, 0.029, 0.0065, 0.0145, 0.0135 ,0.00345],
[0.0155, 0.017, 0.032, 0.0065, 0.0145, 0.0155   ,0.0037],
[0.017, 0.018, 0.0345, 0.007, 0.018, 0.0155    ,0.00405],
[0.0175, 0.019, 0.0375, 0.008, 0.022, 0.0175    ,0.0035],
[0.0185,0.021, 0.0415, 0.008, 0.024, 0.0185     ,0.0038],
[0.0195, 0.0213, 0.045, 0.049, 0.03, 0.021    ,0.00425]
]

#check_0 = ROOT.TH2F("check_0","check_0",50,-1.4,1.4,50,-3.14,3.14)
#check_1 = ROOT.TH2F("check_1","check_1",50,-1.4,1.4,50,-3.14,3.14)
#check_2 = ROOT.TH2F("check_2","check_2",50,-1.4,1.4,50,-3.14,3.14)
#check_3 = ROOT.TH2F("check_3","check_3",50,-1.4,1.4,50,-3.14,3.14)
#check_4 = ROOT.TH2F("check_4","check_4",50,-1.4,1.4,50,-3.14,3.14)
#check_5 = ROOT.TH2F("check_5","check_5",50,-1.4,1.4,50,-3.14,3.14)
#check_6 = ROOT.TH2F("check_6","check_6",50,-1.4,1.4,50,-3.14,3.14)
#check_7 = ROOT.TH2F("check_7","check_7",50,-1.4,1.4,50,-3.14,3.14)
#check_8 = ROOT.TH2F("check_8","check_8",50,-1.4,1.4,50,-3.14,3.14)
#
#check.append(check_0)
#check.append(check_1)
#check.append(check_2)
#check.append(check_3)
#check.append(check_4)
#check.append(check_5)
#check.append(check_6)
#check.append(check_7)
#check.append(check_8)



#Define Trigger# - Pivot for now -
def PivotTrigger(c_x,c_layer,c_station):
 isTrig_3_3_3 = 0
 isTrig_2_2_1 = 0
 isTrig_2_0_1 = 0
 isTrig_1_2_1 = 0
  
 #Define the window to search a trigger
 
 window = [10,10,1,10,10,10,10,20,20]
 
 x_min = -1.1
 x_max = 1.1
 #bin_x = (x_max-x_min) / 1013.
 bin_x = (x_max-x_min) / (512.*2.)

 
 list_perLayer = [[],[],[],[],[],[],[],[],[]]
 for indx,x in enumerate(c_x):

  x_int = int( (x-x_min) / bin_x)
  sta = c_station[indx]
#  check[c_layer[indx]].Fill(c_x,c_x)
  lly = c_layer[indx]
  list_perLayer[lly].append(x_int)

 binnned_x = (c_x-x_min) / bin_x
 #Need to zip the c_x and layer

 
 #loop over the BI to define pivot on layer 2 
 for cell in range(1024): 
 #for indx,x in enumerate(c_x):
   #here I need to define the tower 
   #if math.fabs(x)>1.1:continue
   NTOWERS = 21
   ETARANGE = 2.2
   TOWERSIZE    = 2 * ETARANGE / NTOWERS 
   ETAHALFRANGE =  ETARANGE / 2.

   tower = int((2.0 * ((-1.1+float(cell)/512.) + ETAHALFRANGE) / TOWERSIZE) - 0.5)
      
#0,   1    2   3   4    5    6
#IM1s,IM2s,IOs,MMs,M10s,M20s,OOs



   #x_int = int( (x-x_min) / bin_x)
   x_int = cell 
   #require pivot  
   #if c_layer[indx] == 2 or c_layer[indx] == 1 or c_layer[indx] == 0  : 
   decision = [0,0,0,0,0,0,0,0,0]


#IM1s,IM2s,IOs,MMs,M10s,M20s, ggBI

   for ly in range(9):
     idx_t = -1
     if ly ==0: idx_t = 0 # here need to change
     if ly ==1: idx_t = 0 # here also need to change 
     if ly ==2: idx_t = 0 # here also need to change
     if ly ==3: idx_t = 3
     if ly ==4: idx_t = 3
     if ly ==5: idx_t = 1
     if ly ==6: idx_t = 1
     if ly ==7: idx_t = 4
     if ly ==8: idx_t = 4
     if tower == 21: tower = 20
     if tower < 0: print(tower,x,idx_t)

     Wind = 0
     if sta == "L":
      Wind = int( Windows[tower][idx_t] / bin_x + 0.5) 
     elif sta == "S":
      Wind = int( Windows_small[tower][idx_t] / bin_x + 0.5) 
     #Mean = int( Windows[tower][idx_t] / (2*bin_x))


     #if ly == 2: continue
     mean = x_int
     #mean = x_int + Mean
     for idx_bi in range(mean-Wind,mean+Wind):
      if idx_bi in list_perLayer[ly]:
       decision[ly] = 1
     #and now on the other side
#     for idx_bi in range(-mean-Wind,-mean+Wind):
#      if idx_bi in list_perLayer[ly]:
#       decision[ly] = 1
    #make final trig decision
   hit_bi = 0
   hit_bm = 0
   hit_bo = 0

   if 1 ==  decision[0]: hit_bi = 1 + hit_bi
   if 1 ==  decision[1]: hit_bi = 1 + hit_bi
   if 1 ==  decision[2]: hit_bi = 1 + hit_bi
    
   if 1 ==  decision[3]: hit_bm = 1 + hit_bm
   if 1 ==  decision[4]: hit_bm = 1 + hit_bm
   if 1 ==  decision[5]: hit_bm = 1 + hit_bm
   if 1 ==  decision[6]: hit_bm = 1 + hit_bm

   if 1 ==  decision[7]: hit_bo = 1 + hit_bo
   if 1 ==  decision[8]: hit_bo = 1 + hit_bo
    
   #3/3 hit_bm > 2 and hit_bo > 0
   #if hit_bi > 1: isTrig_2_0_1 = 1
   if (hit_bi > 1 and hit_bo > 0) or ( (hit_bi > 1 and (hit_bm+hit_bo)>2  ) or (hit_bm > 2 and hit_bo> 0)) : isTrig_2_0_1 = 1
   if ( (hit_bi > 1 and (hit_bm+hit_bo)>2  ) or (hit_bm > 2 and hit_bo> 0)) : isTrig_2_2_1 = 1 
   if (  (hit_bm > 2 and hit_bo> 0)) : isTrig_3_3_3 = 1 
   
# File_t = ROOT.TFile("Trig_check.root","recreate")
# check[0].Write() 
# check[1].Write() 
# check[2].Write() 
# check[3].Write() 
# check[4].Write() 
# check[5].Write() 
# check[6].Write() 
# check[7].Write() 
# check[8].Write()
# File_t.Close() 
 return isTrig_2_0_1,isTrig_2_2_1,isTrig_3_3_3


#c_x =     [1,1,1,1,1,1,1,1,1]
##c_x =     [1,100,200,300,400,500,600,700,900]
#c_x = np.asarray(c_x)
#c_layer = [0,1,2,3,4,5,6,7,8]
#c_layer = np.asarray(c_layer)
#T = PivotTrigger(c_x,c_layer)
#
#print(T)
