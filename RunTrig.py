import argparse
import os
import ROOT
import trigger
import numpy as np
import math


f = ROOT.TFile.Open("out.root")
tree = f.Get('tree')


trig_pass_201  = ROOT.TH2F("trig_pass_201","trig_pass_201",  50,-1.4,1.4,50,-3.14,3.14)
trig_total_201 = ROOT.TH2F("trig_total_201","trig_total_201",50,-1.4,1.4,50,-3.14,3.14)

trig_pass_201_pt   = ROOT.TH1F("trig_pass_201_pt","trig_pass_201_pt",20,0,40)
trig_total_201_pt  = ROOT.TH1F("trig_total_201_pt","trig_total_201_pt",20,0,40)


trig_pass_221  = ROOT.TH2F("trig_pass_221","trig_pass_221",  50,-1.4,1.4,50,-3.14,3.14)
trig_total_221 = ROOT.TH2F("trig_total_221","trig_total_221",50,-1.4,1.4,50,-3.14,3.14)

trig_pass_221_pt   = ROOT.TH1F("trig_pass_221_pt","trig_pass_221_pt",20,0,40)
trig_total_221_pt  = ROOT.TH1F("trig_total_221_pt","trig_total_221_pt",20,0,40)

trig_pass_333_pt   = ROOT.TH1F("trig_pass_333_pt","trig_pass_333_pt",20,0,40)
trig_total_333_pt  = ROOT.TH1F("trig_total_333_pt","trig_total_333_pt",20,0,40)

trig_check  = ROOT.TH2F("trig_check","trig_check",  50,-1.4,1.4,50,-3.14,3.14)


i = -1
evn = 0
sel_evn = 0
number_of_tracks = 0
for event in f.tree:
        if evn > 30000: break       
        evn =  evn + 1
        ncl = len(event.x)
        x = event.x 
        mPhi = event.mPhi 
        time = event.time
        layer = event.layer 
        g_match = event.g_match
        station_eta = event.station_eta
        station_phi = event.station_phi
        station_name = event.station_name
        trk_pt = event.trk_pt
        trk_eta = event.trk_eta
        trk_phi = event.trk_phi
        g_eta = event.g_eta
        g_phi = event.g_phi
        #For now use single muon event
        if len(trk_pt) ==0 or len(trk_pt)>1: 
         number_of_tracks = number_of_tracks + 1
         continue
        

        ind1 = np.asarray(mPhi) == 0 
        ind2 = np.asarray(station_name) == "L"
        ind = ind1 
        #ind = ind1*ind2 
        #do not consider small sector
        ncell = 0
        #rude way to remove the Small sector
        for i in ind:
         if i == True: ncell = ncell + 1
        if ncell < 1: continue

        #ind = np.multiply(ind1,ind2) 
        #ind = np.asarray(mPhi) > 0 and np.asarray(station_name) == "L" 
        xt = np.asarray(x)
        lay = np.asarray(layer)
        sta = np.asarray(station_name)

        x4Trig = xt[ind]
        ly4Trig = lay[ind]
        sta4Trig = sta[ind]


        isTrig_2_0_1,isTrig_2_2_1,isTrig_3_3_3 = trigger.PivotTrigger(x4Trig ,ly4Trig,sta4Trig)


        if math.fabs(trk_eta[0]) < 1.0 and trk_phi[0] > 0.0 :              
          if 0 not in ly4Trig or 1 not in ly4Trig or 2 not in ly4Trig:
           trig_check.Fill(trk_eta[0],trk_phi[0])
 
          if isTrig_2_0_1: 
           if trk_pt[0] > 20: trig_pass_201.Fill(trk_eta[0],trk_phi[0])
           trig_pass_201_pt.Fill(trk_pt[0])
          if trk_pt[0] > 20: trig_total_201.Fill(trk_eta[0],trk_phi[0])
          trig_total_201_pt.Fill(trk_pt[0])

          if isTrig_2_2_1: 
            if trk_pt[0] > 20: trig_pass_221.Fill(trk_eta[0],trk_phi[0])
            trig_pass_221_pt.Fill(trk_pt[0])
          if trk_pt[0] > 20: trig_total_221.Fill(trk_eta[0],trk_phi[0])
          trig_total_221_pt.Fill(trk_pt[0])
          if isTrig_3_3_3: 
            trig_pass_333_pt.Fill(trk_pt[0])
          trig_total_333_pt.Fill(trk_pt[0])



File_trig = ROOT.TFile("Trig_results.root","recreate")
File_trig.cd()
trig_pass_221.Divide(trig_total_221)
trig_pass_221.Write()
trig_total_221.Write()

trig_pass_221_pt.Divide(trig_total_221_pt)
trig_pass_221_pt.Write()

trig_pass_201.Divide(trig_total_201)
trig_pass_201.Write()

trig_pass_201_pt.Divide(trig_total_201_pt)
trig_pass_201_pt.Write()

trig_pass_333_pt.Divide(trig_total_333_pt)
trig_pass_333_pt.Write()
trig_check.Write()

